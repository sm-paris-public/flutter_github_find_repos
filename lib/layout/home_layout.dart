import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:lunii_github/widget/home_widget.dart';


class HomeLayout extends StatelessWidget {

  HomeState homeWidget;
  HomeLayout(this.homeWidget);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
          child: Column(children: <Widget>[
            Padding(padding: EdgeInsets.only(top: 30.0)),
            Container(
              padding: EdgeInsets.all(20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Flexible(child: Container(
                    padding: EdgeInsets.only(top: 5),
                    child: _txtFindUser(),
                  )),
                  Padding(padding: EdgeInsets.only(left: 10.0)),
                  Container(
                    child: _btnFindUser(),
                  )
                ]
              )
            ),
            Container(
              child: _infosUser()
            ),
            Expanded(
              child: _builderListView(),
            )
      ]))
    );
  }

  Widget _txtFindUser(){
    return TextField(
      controller: this.homeWidget.nameController,
      decoration:  InputDecoration(
        hintText: "Nom d'utilisateur github",
        contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 10.0, 10.0),
        isDense: true,
        border:  OutlineInputBorder(
            borderSide:  BorderSide(color: Colors.grey)
        ),
      ),
    );
  }

  Widget _btnFindUser(){
    return  RaisedButton.icon(
      onPressed: this.homeWidget.onClickBtnFind,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(10.0))),
      label: Text('Rechercher',
        style: TextStyle(color: Colors.white),),
      icon: Icon(Icons.search, color:Colors.white,),
      textColor: Colors.white,
      splashColor: Colors.red,
      color: Colors.lightBlue
    );
  }

  Widget _infosUser(){
    return Row(
      children: <Widget>[
        Padding(padding: EdgeInsets.only(left: 30.0)),
        CachedNetworkImage(
            width: 50,
            imageUrl: this.homeWidget.user?.avatar ?? ""
        ),
        Padding(padding: EdgeInsets.only(left: 30.0)),
        Text(this.homeWidget.user?.name ?? "" ,
            style: TextStyle(
                fontFamily: 'DancingScript',
                fontSize: 40,
                color: Colors.lightBlue
            ))
      ],
    );
  }

  Widget _builderListView(){
    return ListView.builder(
        padding: const EdgeInsets.all(16.0),
        itemCount: this.homeWidget.datasRepos == null ? 0 : this.homeWidget.datasRepos.length,
        itemBuilder: (context, index){

          var repos = this.homeWidget.datasRepos[index];
          return ListTile(
              title: Text(
                  repos.name ?? "",
                  style: TextStyle(
                      fontFamily: 'DancingScript',
                      fontSize: 20,
                      color: Colors.lightBlue
                    )
              ),
              subtitle: Text(repos.description ?? ""),
            onTap: () => this.homeWidget.onClickCell(repos),
          );
        },
    );
  }


}

