import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:lunii_github/layout/home_layout.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:lunii_github/models/Repos.dart';
import 'package:lunii_github/models/User.dart';
import 'package:url_launcher/url_launcher.dart';


class HomeWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomeState();
  }
}



class HomeState extends State<HomeWidget> {


  TextEditingController nameController = TextEditingController();
  User user;
  List<Repos> datasRepos;



  @override
  Widget build(BuildContext context) {
    // change color statusBar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.blue,
    ));
    return HomeLayout(this);
  }

  /***** Actions *************/

  onClickBtnFind(){
    FocusScope.of(context).requestFocus(FocusNode());
    this.getDataInfosUser();
  }

  onClickCell(Repos data) async{
    if (await canLaunch(data.url)) {
      await launch(data.url, forceSafariVC: false);
    } else {
      throw 'Could not launch ${data.url}';
    }
  }

  /***** WebService *************/

  Future<String> getDataInfosUser() async {
    var response = await http.get(
        Uri.encodeFull("https://api.github.com/users/${this.nameController.text}"),
        headers: {"Accept": "application/json"}
    );

    this.setState(() {
      Map<String, dynamic> map = json.decode(response.body);

      if (map.containsKey("message")){
        user = null;
        this.datasRepos = null;
      }else{
        user = User(map["name"], map["avatar_url"]);
        getDataRepos(map["repos_url"]);
      }
    });
    return "Successfull";
  }

  Future<String> getDataRepos(url) async {
    var response = await http.get(
        Uri.encodeFull(url),
        headers: {"Accept": "application/json"}
    );

    this.setState(() {
      List data = json.decode(response.body);

      this.datasRepos = new List<Repos>();
      for(var name in data){
        this.datasRepos.add(Repos(name["id"],name["name"],name["description"],name["html_url"]));
      }

      print(this.datasRepos.length);

    });

    return "Successfull";
  }

}