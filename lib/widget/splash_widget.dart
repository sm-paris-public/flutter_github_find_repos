import 'dart:async';

import 'package:flutter/material.dart';
import 'package:lunii_github/layout/splash_layout.dart';
import 'package:flutter/services.dart';
import 'package:lunii_github/widget/home_widget.dart';


class SplashWidget extends StatefulWidget{

  @override
  State<StatefulWidget> createState() {
    return StartState();
  }

}


class StartState extends State<SplashWidget> {
  @override
  Widget build(BuildContext context) {
    // change color statusBar
    SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
      statusBarColor: Colors.blue,
    ));

    return SplashLayout();
  }


  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    return new Timer(Duration(seconds: 4), gotoNextPage);
  }

  gotoNextPage() {
    Navigator.pushReplacement(context, MaterialPageRoute(
        builder: (context) => HomeWidget()
    )
    );
  }

}