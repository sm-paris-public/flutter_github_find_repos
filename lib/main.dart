import 'package:flutter/material.dart';
import 'package:lunii_github/widget/splash_widget.dart';



void main() =>  runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    FocusScope.of(context).requestFocus(FocusNode());
    return MaterialApp(
        debugShowCheckedModeBanner : false,
      home: SplashWidget()
    );
  }

}